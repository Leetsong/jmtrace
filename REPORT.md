## jmtrace

> DZ1833012 李聪

#### 1 Introduction

This experiment is the second program assignment of course “Introduction to Software Engineering Research”, aiming to output the memory access information for a packaged JAVA program (*\*.jar*). For each memory access, it is expected to output a log entry of the following format,

```txt
<R|W> <tid> <hash> <array_index|field_name>
```

For simplicity, name them, respectively, as `action`, `tid`, `hash`, and `name`.

#### 2 Analysis and Design

**Action** and **Tid**. Actually, it is trivial to know the action, and thread id.

**Hash**. To get the hash value, the instrumentation should take a bit more efforts.

+ `GETSTATIC indexbyte1 indexbyte2`, and `*ALOAD`: These instructions load onto the stack a `value`. Hence, to get the hash of `value`, one can instrument a `DUP` or `DUP2` instruction immediately *after* it, and invoke `System.identifyHashCode()`.

+ `PUTSTATIC indexbyte1 indexbyte2`, and `*ASTORE`: These instruction put a  `value` on stack to otherwhere. Therefore, to get the hash of `value`, one can instrument a `DUP` or `DUP2` instruction immediately *before* it, and invoke `System.identifyHashCode()`.

**Name**. It is trivial to get name of `GETSTATIC`, `PUTSTATIC`, `GETFIELD`, and `PUTFIELD`. However, it is non-trivial for `*ALOAD` and `*ASTORE`. However, one know that `*ALOAD` and `*ASTORE` aims at loading/storing array values. These two-family instructions take an `arrayref` from the operand stack. Therefore, one can duplicate the `arrayref`, and get its type via reflection *before* instructions are executed. Specifically, one instruments a `DUP` instruction immediately *before* them, and invoke `Class.getClass().getTypeName()`.

#### 3 Implementation

jmtrace is based on ASM [2], and javaagent [3]. One should take care about the stack map frame when implementing jmtrace, especially inserting new local variables. If the local variable is newed but not initialized, the stack frame map is `TOP`, which always fails to pass the verification checking when it is embedded in a loop. *A good practice to insert local variables is to new and initialize it*.

#### 4 Experiment

We test jmtrace on `apktool`, `smali` and `baksmali`, which are widely used in Android reverse engineering. The following presents the experiment results from `apktool` and `baksmali`.

<img width='600' src='/home/simon/Desktop/apktool.png' />

<img width='600' src='/home/simon/Desktop/baksmali.png' />

#### 5 Conclusion

This program assignment implements a java memory access monitoring tool named jmtrace.

#### References

[1] The Java Virtual Machine Instruction Set. https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html

[2] ASM - A Java bytecode engineering library. https://asm.ow2.io/

[3] javaagent. https://docs.oracle.com/javase/7/docs/api/java/lang/instrument/package-summary.html