# jmtrace

## 1 Prerequisite

+ Java:  >= 8

## 2 Install

``` bash
$ cd code
$ ./install <path/to/dest/dir>
```

## 3 Run

```bash
# cd to <path/to/dest/dir>
$ ./jmtrace <args>  # <args> is any arg of java command except -javaagent
```

