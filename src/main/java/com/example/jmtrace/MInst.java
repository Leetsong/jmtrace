package com.example.jmtrace;

import org.objectweb.asm.*;
import org.objectweb.asm.commons.LocalVariablesSorter;

final public class MInst extends MethodVisitor {

    private boolean mInitialized = false;
    private LocalVariablesSorter mSorter;

    private int mActionStrIndex;
    private int mTIdStrIndex;
    private int mHashStrIndex;
    private int mNameStrIndex;

    private int mATypeStrIndex;
    private int mAIndexStrIndex;
    private int mAIndexIndex;
    private int mARefIndex;

    private int mTmpIIndex;
    private int mTmpLIndex;
    private int mTmpFIndex;
    private int mTmpDIndex;
    private int mTmpAIndex;

    public MInst(MethodVisitor mv) {
        super(Opcodes.ASM7, mv);
    }

    public void setLocalVarSorter(LocalVariablesSorter sorter) {
        this.mSorter = sorter;
    }

    @Override
    public void visitCode() {
        super.visitCode();
        initLocalVarsIfNot();
    }

    @Override
    public void visitInsn(int opcode) {
        boolean succeeded = true;

        // save array index, and array ref
        switch (opcode) {
            case Opcodes.IALOAD: case Opcodes.LALOAD: case Opcodes.FALOAD: case Opcodes.DALOAD:
            case Opcodes.AALOAD: case Opcodes.BALOAD: case Opcodes.CALOAD: case Opcodes.SALOAD:
                super.visitInsn(Opcodes.DUP2);
                super.visitVarInsn(Opcodes.ISTORE, mAIndexIndex);
                super.visitVarInsn(Opcodes.ASTORE, mARefIndex);
                break;

            case Opcodes.BASTORE: case Opcodes.CASTORE: case Opcodes.SASTORE:
            case Opcodes.IASTORE: saveArrayIndexRef(Opcodes.ISTORE, Opcodes.ILOAD, mTmpIIndex); break;
            case Opcodes.LASTORE: saveArrayIndexRef(Opcodes.LSTORE, Opcodes.LLOAD, mTmpLIndex); break;
            case Opcodes.FASTORE: saveArrayIndexRef(Opcodes.FSTORE, Opcodes.FLOAD, mTmpFIndex); break;
            case Opcodes.DASTORE: saveArrayIndexRef(Opcodes.DSTORE, Opcodes.DLOAD, mTmpDIndex); break;
            case Opcodes.AASTORE: saveArrayIndexRef(Opcodes.ASTORE, Opcodes.ALOAD, mTmpAIndex); break;

            default:
                succeeded = false;
        }

        // run opcode
        super.visitInsn(opcode);

        if (!succeeded) {
            return;
        }

        // compute name using array index and array ref
        computeName();

        // prepare hash
        String type = null;
        super.visitVarInsn(Opcodes.ALOAD, mARefIndex);
        super.visitVarInsn(Opcodes.ILOAD, mAIndexIndex);
        switch (opcode) {
            case Opcodes.IALOAD: case Opcodes.IASTORE: super.visitInsn(Opcodes.IALOAD); type = "I"; break;
            case Opcodes.LALOAD: case Opcodes.LASTORE: super.visitInsn(Opcodes.LALOAD); type = "J"; break;
            case Opcodes.FALOAD: case Opcodes.FASTORE: super.visitInsn(Opcodes.FALOAD); type = "F"; break;
            case Opcodes.DALOAD: case Opcodes.DASTORE: super.visitInsn(Opcodes.DALOAD); type = "D"; break;
            case Opcodes.AALOAD: case Opcodes.AASTORE: super.visitInsn(Opcodes.AALOAD); type = "Ljava/lang/Object;"; break;
            case Opcodes.BALOAD: case Opcodes.BASTORE: super.visitInsn(Opcodes.BALOAD); type = "B"; break;
            case Opcodes.CALOAD: case Opcodes.CASTORE: super.visitInsn(Opcodes.CALOAD); type = "C"; break;
            case Opcodes.SALOAD: case Opcodes.SASTORE: super.visitInsn(Opcodes.SALOAD); type = "S"; break;
        }
        instrument(opcode <= Opcodes.SALOAD ? "R" : "W", null, type);

        if (MTrace.OPT_DEBUG_FLAG) {
            System.out.println((opcode <= Opcodes.SALOAD ? "R" : "W") + " array [+" + type);
        }
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
        String fullName = Helper.slashesToDots(owner) + "." + name;

        if (MTrace.OPT_DEBUG_FLAG) {
            System.out.println((opcode == Opcodes.GETSTATIC || opcode == Opcodes.GETFIELD ? "R" : "W") +
                    " field|static " + fullName);
        }

        switch (opcode) {
            case Opcodes.GETSTATIC:
                // run GETSTATIC
                super.visitFieldInsn(opcode, owner, name, descriptor);
                // prepare hash
                super.visitFieldInsn(Opcodes.GETSTATIC, owner, name, descriptor);
                instrument("R", fullName, descriptor);
                break;

            case Opcodes.PUTSTATIC:
                // run PUTSTATIC
                super.visitFieldInsn(opcode, owner, name, descriptor);
                // prepare hash
                super.visitFieldInsn(Opcodes.GETSTATIC, owner, name, descriptor);
                instrument("W", fullName, descriptor);
                break;

            case Opcodes.GETFIELD:
                // run GETFIELD
                super.visitFieldInsn(opcode, owner, name, descriptor);
                // prepare hash
                switch (descriptor) {
                    case "J":
                    case "D": super.visitInsn(Opcodes.DUP2); break;
                    default:  super.visitInsn(Opcodes.DUP);
                }
                instrument("R", fullName, descriptor);
                break;

            case Opcodes.PUTFIELD: {
                // save new object value
                switch (descriptor) {
                    case "B": case "Z": case "C": case "S":
                    case "I": saveNewValue(Opcodes.ISTORE, mTmpIIndex); break;
                    case "F": saveNewValue(Opcodes.FSTORE, mTmpFIndex); break;
                    case "J": saveNewValue2(Opcodes.LSTORE, mTmpLIndex); break;
                    case "D": saveNewValue2(Opcodes.DSTORE, mTmpDIndex); break;
                    default:  saveNewValue(Opcodes.ASTORE, mTmpAIndex);
                }
                // run PUTFIELD
                super.visitFieldInsn(opcode, owner, name, descriptor);
                // prepare hash
                switch (descriptor) {
                    case "B": case "Z": case "C": case "S":
                    case "I": super.visitVarInsn(Opcodes.ILOAD, mTmpIIndex); break;
                    case "F": super.visitVarInsn(Opcodes.FLOAD, mTmpFIndex); break;
                    case "J": super.visitVarInsn(Opcodes.LLOAD, mTmpLIndex); break;
                    case "D": super.visitVarInsn(Opcodes.DLOAD, mTmpDIndex); break;
                    default:  super.visitVarInsn(Opcodes.ALOAD, mTmpAIndex);
                }
                instrument("W", fullName, descriptor);
                break;
            }
        }
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals) {
        if (mInitialized) {
            // expand stack and locals
            super.visitMaxs(maxStack + 6, maxLocals + 14);
        } else {
            super.visitMaxs(maxStack, maxLocals);
        }
    }

    private void initLocalVarsIfNot() {
        if (mInitialized) {
            return;
        }

        mActionStrIndex = newAndInitStringLocalVar();  // 0
        mTIdStrIndex = newAndInitStringLocalVar();     // 1
        mHashStrIndex = newAndInitStringLocalVar();    // 2
        mNameStrIndex = newAndInitStringLocalVar();    // 3

        mATypeStrIndex = newAndInitStringLocalVar();   // 4
        mAIndexStrIndex = newAndInitStringLocalVar();  // 5
        mAIndexIndex = newAndInitIntLocalVar();        // 6
        mARefIndex = newAndInitObjectLocalVar();;      // 7

        mTmpIIndex = newAndInitIntLocalVar();          // 8
        mTmpLIndex = newAndInitLongLocalVar();         // 9
        mTmpFIndex = newAndInitFloatLocalVar();        // 11
        mTmpDIndex = newAndInitDoubleLocalVar();       // 12
        mTmpAIndex = newAndInitObjectLocalVar();;      // 14

        mInitialized = true;
    }

    private int newAndInitIntLocalVar() {
        int index = mSorter.newLocal(Type.INT_TYPE);
        super.visitLdcInsn(1);
        super.visitVarInsn(Opcodes.ISTORE, index);
        return index;
    }

    private int newAndInitLongLocalVar() {
        int index = mSorter.newLocal(Type.LONG_TYPE);
        super.visitLdcInsn(1L);
        super.visitVarInsn(Opcodes.LSTORE, index);
        return index;
    }

    private int newAndInitFloatLocalVar() {
        int index = mSorter.newLocal(Type.FLOAT_TYPE);
        super.visitLdcInsn(1F);
        super.visitVarInsn(Opcodes.FSTORE, index);
        return index;
    }

    private int newAndInitDoubleLocalVar() {
        int index = mSorter.newLocal(Type.DOUBLE_TYPE);
        super.visitLdcInsn(1D);
        super.visitVarInsn(Opcodes.DSTORE, index);
        return index;
    }

    private int newAndInitStringLocalVar() {
        int index = mSorter.newLocal(Type.getType(String.class));
        super.visitLdcInsn("S");
        super.visitVarInsn(Opcodes.ASTORE, index);
        return index;
    }

    private int newAndInitObjectLocalVar() {
        int index = mSorter.newLocal(Type.getType(Object.class));
        super.visitLdcInsn("O");
        super.visitVarInsn(Opcodes.ASTORE, index);
        return index;
    }

    private void instrument(String action, String name, String descriptor) {
        // hash: must be prepared before instrumenting
        convertToObject(descriptor);
        super.visitMethodInsn(Opcodes.INVOKESTATIC,
                "java/lang/System",
                "identityHashCode",
                "(Ljava/lang/Object;)I",
                false);
        super.visitInsn(Opcodes.I2L);
        super.visitMethodInsn(Opcodes.INVOKESTATIC,
                "java/lang/Long",
                "toHexString",
                "(J)Ljava/lang/String;",
                false);
        super.visitVarInsn(Opcodes.ASTORE,
                mHashStrIndex);

        // action
        super.visitLdcInsn(action);
        super.visitVarInsn(Opcodes.ASTORE,
                mActionStrIndex);

        // name
        if (name != null) {
            super.visitLdcInsn(name);
            super.visitVarInsn(Opcodes.ASTORE,
                    mNameStrIndex);
        }

        // thread id
        super.visitMethodInsn(Opcodes.INVOKESTATIC,
                "java/lang/Thread",
                "currentThread",
                "()Ljava/lang/Thread;",
                false);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/Thread",
                "getId",
                "()J",
                false);
        super.visitMethodInsn(Opcodes.INVOKESTATIC,
                "java/lang/Long",
                "toString",
                "(J)Ljava/lang/String;",
                false);
        super.visitVarInsn(Opcodes.ASTORE,
                mTIdStrIndex);

        // create and initialize an empty String
        super.visitFieldInsn(Opcodes.GETSTATIC,
                "java/lang/System",
                "out",
                "Ljava/io/PrintStream;");
        super.visitTypeInsn(Opcodes.NEW,
                "java/lang/StringBuilder");
        super.visitInsn(Opcodes.DUP);
        super.visitMethodInsn(Opcodes.INVOKESPECIAL,
                "java/lang/StringBuilder",
                "<init>",
                "()V",
                false);

        // assemble them to "action thread_id hash name"
        // append action
        super.visitVarInsn(Opcodes.ALOAD,
                mActionStrIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitLdcInsn(" ");
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        // append thread id
        super.visitVarInsn(Opcodes.ALOAD,
                mTIdStrIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitLdcInsn(" ");
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        // append hash
        super.visitLdcInsn("00000000");
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitVarInsn(Opcodes.ALOAD,
                mHashStrIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitLdcInsn(" ");
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        // append name
        super.visitVarInsn(Opcodes.ALOAD,
                mNameStrIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);

        // toString, and println
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "toString",
                "()Ljava/lang/String;",
                false);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/io/PrintStream",
                "println",
                "(Ljava/lang/String;)V",
                false);
    }

    private void computeName() {
        // get index
        super.visitVarInsn(Opcodes.ILOAD, mAIndexIndex);
        super.visitMethodInsn(Opcodes.INVOKESTATIC,
                "java/lang/Integer",
                "toString",
                "(I)Ljava/lang/String;",
                false);
        super.visitVarInsn(Opcodes.ASTORE, mAIndexStrIndex);

        // get type
        super.visitVarInsn(Opcodes.ALOAD, mARefIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/Object",
                "getClass",
                "()Ljava/lang/Class;",
                false);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/Class",
                "getTypeName",
                "()Ljava/lang/String;",
                false);
        super.visitVarInsn(Opcodes.ASTORE, mATypeStrIndex);

        // name = type[:-1] + index + "]"
        super.visitTypeInsn(Opcodes.NEW,
                "java/lang/StringBuilder");
        super.visitInsn(Opcodes.DUP);
        super.visitMethodInsn(Opcodes.INVOKESPECIAL,
                "java/lang/StringBuilder",
                "<init>",
                "()V",
                false);
        super.visitVarInsn(Opcodes.ALOAD,
                mATypeStrIndex);
        super.visitInsn(Opcodes.ICONST_0);
        super.visitVarInsn(Opcodes.ALOAD,
                mATypeStrIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/String",
                "length",
                "()I",
                false);
        super.visitInsn(Opcodes.ICONST_1);
        super.visitInsn(Opcodes.ISUB);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/String",
                "substring",
                "(II)Ljava/lang/String;",
                false);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitVarInsn(Opcodes.ALOAD, mAIndexStrIndex);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitLdcInsn("]");
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                false);
        super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                "java/lang/StringBuilder",
                "toString",
                "()Ljava/lang/String;",
                false);
        super.visitVarInsn(Opcodes.ASTORE, mNameStrIndex);
    }

    private void saveNewValue(int storeOp, int tmpIndex) {
        super.visitInsn(Opcodes.DUP);
        super.visitVarInsn(storeOp, tmpIndex);
    }

    private void saveNewValue2(int storeOp, int tmpIndex) {
        super.visitInsn(Opcodes.DUP2);
        super.visitVarInsn(storeOp, tmpIndex);
    }

    private void saveArrayIndexRef(int storeOp, int loadOp, int tmpIndex) {
        super.visitVarInsn(storeOp, tmpIndex);
        super.visitInsn(Opcodes.DUP2);
        super.visitVarInsn(Opcodes.ISTORE, mAIndexIndex);
        super.visitVarInsn(Opcodes.ASTORE, mARefIndex);
        super.visitVarInsn(loadOp, tmpIndex);
    }

    private void convertToObject(String descriptor) {
        switch (descriptor) {
            case "Z": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Boolean",
                        "valueOf",
                        "(Z)Ljava/lang/Boolean;",
                        false);
                break;
            }
            case "C": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Character",
                        "valueOf",
                        "(C)Ljava/lang/Character;",
                        false);
                break;
            }
            case "B": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Byte",
                        "valueOf",
                        "(B)Ljava/lang/Byte;",
                        false);
                break;
            }
            case "S": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Short",
                        "valueOf",
                        "(S)Ljava/lang/Short;",
                        false);
                break;
            }
            case "I": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Integer",
                        "valueOf",
                        "(I)Ljava/lang/Integer;",
                        false);
                break;
            }
            case "F": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Float",
                        "valueOf",
                        "(F)Ljava/lang/Float;",
                        false);
                break;
            }
            case "J": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Long",
                        "valueOf",
                        "(J)Ljava/lang/Long;",
                        false);
                break;
            }
            case "D": {
                super.visitMethodInsn(Opcodes.INVOKESTATIC,
                        "java/lang/Double",
                        "valueOf",
                        "(D)Ljava/lang/Double;",
                        false);
            }
        }
    }

}
