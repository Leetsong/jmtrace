package com.example.jmtrace;

import org.objectweb.asm.*;
import org.objectweb.asm.commons.LocalVariablesSorter;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.instrument.Instrumentation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MTrace {

    public static boolean OPT_DEBUG_FLAG = false;
    private static boolean OPT_DEBUG_TRACE_FLAG = false;

    public static void premain(String args, Instrumentation instrumentation) {
        parseOptions(args);
        instrumentation.addTransformer((classLoader, className, theClass, protectionDomain, bytes) -> {
            if (BLACKLIST.contain(Helper.slashesToDots(className))) {
                return bytes;
            }
            return instrumentMemoryAccessInsn(new ClassReader(bytes));
        });
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            System.out.println("Error: no bytecode file provided, or no output file provided");
            System.out.println("Usage: java -jar jmtrace.jar <bytecode_file> <output_file> [debug[,trace]]");
            System.exit(1);
        }
        String input = args[0];
        String output = args[1];
        if (args.length >= 3) {
            parseOptions(args[2]);
        }
        ClassReader reader = new ClassReader(new FileInputStream(input));
        byte[] bytes = instrumentMemoryAccessInsn(reader);
        FileOutputStream outputStream = new FileOutputStream(output);
        outputStream.write(bytes);
        outputStream.close();
        System.out.println("Instrumented class written to " + output);
    }

    private static void parseOptions(String args) {
        if (args != null && args.length() != 0) {
            Set<String> options = new HashSet<>(Arrays.asList(args.split(",")));
            if (options.contains("debug")) {
                OPT_DEBUG_FLAG = true;
            }
            if (options.contains("trace")) {
                OPT_DEBUG_TRACE_FLAG = true;
            }
        }
    }

    private static byte[] instrumentMemoryAccessInsn(ClassReader reader) {
        // reader
        // -> [tracer ->] instrumentation
        // -> [checker -> tracer -> visionor ->] writer
        ClassWriter writer = new ClassWriter(0);
        ClassVisitor visitor = writer;

        // first visionor->tracer
        if (OPT_DEBUG_FLAG) {
            visitor = new CVisionor(visitor);
            if (OPT_DEBUG_TRACE_FLAG) { // last visitor
                visitor = new TraceClassVisitor(visitor, new PrintWriter(System.out));
            }
            visitor = new CheckClassAdapter(visitor);
        }

        // instrumentation
        visitor = new ClassVisitor(Opcodes.ASM7, visitor) {
            private boolean mIsInterface = true;

            @Override
            public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                super.visit(version, access, name, signature, superName, interfaces);
                mIsInterface = (access & Opcodes.ACC_INTERFACE) != 0;
            }

            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
                if (mIsInterface || mv == null) {
                    return mv;
                }
                MInst minst = new MInst(mv);
                LocalVariablesSorter sorter = new LocalVariablesSorter(access, descriptor, minst);
                minst.setLocalVarSorter(sorter);
                return sorter;
            }
        };

        // last tracer
        if (OPT_DEBUG_FLAG && OPT_DEBUG_TRACE_FLAG) { // last visitor
            visitor = new TraceClassVisitor(visitor, new PrintWriter(System.out));
        }

        reader.accept(visitor, ClassReader.EXPAND_FRAMES);

        return writer.toByteArray();
    }
}
