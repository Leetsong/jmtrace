package com.example.jmtrace;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

final public class BLACKLIST {

    public static final Set<String> blackList = new HashSet<>(Arrays.asList(
            "java.",
            "javax.",
            "javafx.",
            "com.sun.",
            "jdk.",
            "netscape.javascript.",
            "org.ietf.jgss.",
            "org.omg.",
            "org.w3c.",
            "org.xml.",
            "sun."
    ));

    public static boolean contain(String className) {
        if (className == null) {
            return true;
        }
        for (String s : blackList) {
            if (className.startsWith(s)) {
                return true;
            }
        }
        return false;
    }
}
