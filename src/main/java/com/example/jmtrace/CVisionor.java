package com.example.jmtrace;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

final public class CVisionor extends ClassVisitor {

    public CVisionor(ClassVisitor cv) {
        super(Opcodes.ASM7, cv);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        System.out.print(".class " + name + " ext " + superName);
        if (interfaces.length != 0) {
            System.out.print(" imp " + String.join(", ", interfaces));
        }
        System.out.println();
        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public void visitNestMember(String nestMember) {
        System.out.println(" .nest " + nestMember);
        super.visitNestMember(nestMember);
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        System.out.println(" .field " + name + " " + descriptor);
        return super.visitField(access, name, descriptor, signature, value);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        System.out.println(" .method " + name + " " + descriptor);
        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }

    @Override
    public void visitEnd() {
        System.out.println(".end\n");
        super.visitEnd();
    }
}
