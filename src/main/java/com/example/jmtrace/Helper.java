package com.example.jmtrace;

public class Helper {

    public static String slashesToDots(String slashed) {
        if (slashed == null) {
            return null;
        }
        return slashed.replace("/", ".");
    }
}
